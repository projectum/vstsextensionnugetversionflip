
Trace-VstsEnteringInvocation $MyInvocation

function FindFile ($folderPath, $fileExtension, $recursive = $false) {
    if ($recursive) {
        $dir = get-childitem $folderPath -recurse
    }
    else {
        $dir = get-childitem $folderPath 
    }

    return $dir | Where-Object {$_.extension -eq $FileExtension}
}

try {
    $inputNugetFile = Get-VstsInput -Name 'nugetFile' -Require
    $inputPackageType = Get-VstsInput -Name 'packType' -Require

    $inputPackageType = "$inputPackageType".ToLowerInvariant()

    if ($inputPackageType -eq "release") {
        Write-Host "Release version selected. Package won't be touched."
        return;
    }

    Assert-VstsPath -LiteralPath $inputNugetFile 

    $tempDirectory = Get-VstsTaskVariable -Name 'agent.tempDirectory' -Require

    Write-Debug $tempDirectory
    Assert-VstsPath -LiteralPath $tempDirectory -PathType 'Container'
    $extractPath = [System.IO.Path]::Combine($tempDirectory, "$([System.Guid]::NewGuid())")

    Add-Type -A System.IO.Compression.FileSystem

    [IO.Compression.ZipFile]::ExtractToDirectory($inputNugetFile, $extractPath)

    $nuspecFile = FindFile -folderPath $extractPath -fileExtension '.nuspec'

    Assert-VstsPath -LiteralPath $nuspecFile.FullName
       
    $nuspecXml = [xml](Get-Content $nuspecFile.FullName)

    Write-Host $nuspecXml

    $nuspecXml.package.metadata.version = "$($nuspecXml.package.metadata.version)-$($inputPackageType)"
    $nuspecXml.Save($nuspecFile.FullName)


    $systemFiles = FindFile -folderPath $extractPath -fileExtension '.psmdcp' -recursive $true

    foreach($systemFile in $systemFiles){
        $systemFileContent = [xml](Get-Content $systemFile.FullName)
        if($systemFileContent.coreProperties){
            $systemFileContent.coreProperties.version = "$($systemFileContent.coreProperties.version)-$($inputPackageType)"
            $systemFileContent.Save($systemFile.FullName)
        }
    }

    Remove-Item $inputNugetFile

    [IO.Compression.ZipFile]::CreateFromDirectory($extractPath, $inputNugetFile)
}
finally {
    Trace-VstsLeavingInvocation $MyInvocation
}